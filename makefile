LIBS = -lcrypto -pthread

rainbow: rainbow.o
	g++ -o $@ $^ $(LIBS)

rainbow.o: rainbow.cpp
	g++ -Wall -c rainbow.cpp $(LIBS)

all: run

run: rainbow
	./rainbow

clean:
	-rm -f *.o
